//
//  ViewController.swift
//  Ejemplo Control Robot - ESADE
//
//  Created by Francesc Roig on 2018
//
//  Info Websocket library -> https://github.com/daltoniam/Starscream
//

//  Comandos que se pueden enviar al robot para controlarlo.
//
//  "run_forward" -> Adelante indefinidamente.
//  "run_backward" -> Atras indefinidamente.
//  "run_forward_l" -> Adelante indefinidamente motor izquierdo.
//  "run_forward_r" -> Adelante indefinidamente motor derecho.
//  "run_backward_l" -> Atras indefinidamente motor izquierdo.
//  "run_backward_r" -> Atras indefinidamente motor derecho.
//  "run_r" -> Gira a la derecha indefinidamente.
//  "run_l" -> Gira a la izquierda indefinidamente.
//  "stop" -> Detiene los motores.
//  "stop_l" -> Detiene el motor izquierdo.
//  "stop_r" -> Detiene el motor derecho.
//
//  Ajusta la potencia de los motores. Los rango de valores permitidos son de 0 a 1023.
//  "power_<0...1023>" -> Ejemplo: 'speed_1000' ajusta la velocidad de los motores a 1000.
//
//  "powerr_<0...1023>" y "powerl_<0...1023>" -> Ajusta la potencia independientemente de cada. motor.


import UIKit

// Aquí se debe especificar la dirección IP del robot,
// la que aparece en el display trasero de éste.
// let IP_ROBOT:String = "minibot.local"
let IP_ROBOT:String = "192.168.10.103"

// Aquí seleccionamos que motor se quire controlar:
// "R" para derecho o "L" para el izquierdo.
let MOTOR_CONTROL = "R"


/**
 * Controlador de la pantalla inicial.
 */
class ViewController: UIViewController, WebSocketDelegate {
    
    // Declara el componente socket que permite enviar y recibir mensajes
    // del robot a través del protocolo WebSocket.
    var socket: WebSocket!
    
    // Etiqueta que del título. Muestra también que motor controla.
    @IBOutlet weak var etiquetaTitulo: UILabel!
    
    
    /*
     * Función que se ejecuta en el momento en que se carga la
     * pantalla asociada a este controlador.
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Cambiamos el texto segun qué motor se controla.
        if MOTOR_CONTROL == "R" {
            etiquetaTitulo.text = "Control Motor Right"
        } else {
            etiquetaTitulo.text = "Control Motor Left"
        }
        
        // Se configura la conexión del websocket.
        var request = URLRequest(url: URL(string: "http://" + IP_ROBOT + ":81")!)
        request.timeoutInterval = 4
        socket = WebSocket(request: request)
        socket.delegate = self
        socket.connect()
    }
    
    
    /*
     * Función que se invoca cuando se ha conseguido establecer la
     * conexión websocket con el robot. Permite detectar que se
     * ha estabecido tal conexión.
     */
    func websocketDidConnect(socket: WebSocketClient) {
        print("websocket is connected")
        etiquetaTitulo.textColor = UIColor.blue
    }
    
    
    /*
     * Función que se invoca cuando se desconectado o perdido la
     * conexión websocket con el robot. Permite detectar que se
     * perdido la conexión.
     */
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        if let e = error as? WSError {
            print("websocket is disconnected: \(e.message)")
        } else if let e = error {
            print("websocket is disconnected: \(e.localizedDescription)")
        } else {
            print("websocket disconnected")
        }
        etiquetaTitulo.textColor = UIColor.red
        socket.connect()
    }
    
    
    /*
     * Función que se invoca cuando se recibe algun mensaje en formato String
     a a través de la conexión websocket con el robot. Permite obtener el mensaje
     * recibido a través del parámetro 'text' de la función.
     */
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        print("Received text: \(text)")
    }
    
    
    /*
     * Función que se invoca cuando se recibe datos a través de la conexión websocket
     * con el robot. Permite obtener los datos recibidos a través del parámetro 'data'
     * de la función.
     */
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        print("Received data: \(data.count)")
    }
    
    
    /*
     * Responde a la pulsación del botón Forward.
     * (evento de presionar el botón 'Touch Down')
     */
    @IBAction func accionBotonForward(_ sender: Any) {
        // Selecciona el comando a mandar al motor segun el lado.
        if MOTOR_CONTROL == "R" {
            socket.write(string: "run_forward_r")
        } else {
            socket.write(string: "run_forward_l")
        }
    }
    
    
    /*
     * Responde a la pulsación del botón Stop.
     * (evento de presionar el botón 'Touch Down')
     */
    @IBAction func accionBotonStop(_ sender: Any) {
        // Selecciona el comando a mandar al motor segun el lado.
        if MOTOR_CONTROL == "R" {
            socket.write(string: "stop_r")
        } else {
            socket.write(string: "stop_l")
        }
    }
    
    
    /*
     * Responde a la pulsación del botón Backward.
     * (evento de presionar el botón 'Touch Down')
     */
    @IBAction func accionBotonBackward(_ sender: Any) {
        // Selecciona el comando a mandar al motor segun el lado.
        if MOTOR_CONTROL == "R" {
            socket.write(string: "run_backward_r")
        } else {
            socket.write(string: "run_backward_l")
        }
    }
    
    
    /*
     * Responde al desplazamiento del Slider de control de la potencia
     * del motor (evento de presionar el botón 'Value Changed').
     * (en el momento en que se deja de mover el slider, se genera el
     * evento que ejecuta esta función).
     */
    @IBAction func accionCambioPotenciaMotor(_ sender: UISlider) {
        // Lee el valor del slider segun su posición.
        let potencia_motor = Int(sender.value)
        
        if MOTOR_CONTROL == "R" {
            socket.write(string: "powerr_\(potencia_motor)")
        } else {
            socket.write(string: "powerl_\(potencia_motor)")
        }
    }
    
    
} // Final del bloque principal 'ViewController'.
